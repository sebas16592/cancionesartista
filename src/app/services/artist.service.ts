import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { RootObject } from '../artist/interfaces/artist.interface';
import { Search } from '../artist/interfaces/search.interface';
import { RootObject2 } from '../artist/interfaces/sound.interface';
import { environment } from '../../environments/environment';
import { TokenSpotify } from '../artist/interfaces/token.interface';

@Injectable({
  providedIn: 'root'
})
export class ArtistService {

  constructor(private http: HttpClient) { }

  private _history: string[] = [];
  public result: Search[] = [];
  public artist: Search;
  public tracks: RootObject2;
  private token: string = "";
  private client_id: string= environment.client_id;
  private client_secret: string =environment.client_secret;
  private redirect_uri: string= environment.utl;
  get history() {    
    return [...this._history];
  }

  searchArtist(query: string) { 
    
    query = query.trim().toLowerCase();
    if ( !this._history.includes(query) ) {
      this._history.unshift( query );
      this._history = this._history.slice(0,10);
    }
    this.http.get(`https://api.spotify.com/v1/search?q=${query}&type=artist&market=US&limit=10&offset=5`, {
      headers: new HttpHeaders().set('Authorization', this.token)
    })    
      .subscribe( (resp : RootObject) =>{
        this.result = [];
        resp.artists.items.forEach(element => {   
          this.result.push({
            "name" : element? element.name : null, 
            "id": element? element.id : null, 
            "image": element.images[0]? element.images[0].url: null, 
            "followers" : element.followers, 
            "genres":element.genres, 
            "url":element.external_urls.spotify
          });  
              
        });
      }); 
  }

  searchArtinstSound(value: string) {
    

    this.http.get(`https://api.spotify.com/v1/artists/${value}/top-tracks?market=ES`,  { 
      headers: new HttpHeaders().set('Authorization', this.token)
    })
    .subscribe( (resp : RootObject2) =>{
        this.tracks = resp;
    }); 
  }

  obtenertoken(code:string) {
    const body = new HttpParams()
      .set('code', code)
      .set('grant_type','authorization_code')
      .set('redirect_uri',this.redirect_uri)
      .set('client_id',this.client_id)
      .set('client_secret',this.client_secret);
    this.http.post(`https://accounts.spotify.com/api/token`, 
    body.toString(), {
      headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
    }).subscribe( (resp: TokenSpotify) => {
      this.token = resp.token_type+" "+resp.access_token;
    })
  }

  requestAuthorization(){
    
    let url = "https://accounts.spotify.com/authorize";

    url += "?client_id=" + this.client_id;
    url += "&response_type=code";
    url += "&redirect_uri=" + encodeURI(this.redirect_uri);
    url += "&show_dialog=false";
    url += "&scope=user-read-private%20user-read-email"
    window.location.href = url;
  
  }
  
  searchAdd(value: Search){
    this.artist = value;
  }
}
