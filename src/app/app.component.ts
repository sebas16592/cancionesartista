import { Component, OnInit } from '@angular/core';
import { ArtistService } from './services/artist.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  constructor (private artistService:ArtistService){

  }
  public prodId: string = this.getParameterByName('code');
  ngOnInit(): void {
    if (this.prodId == "") {
      this.artistService.requestAuthorization();
      
    } else {
      this.artistService.obtenertoken(this.prodId);
    }
    
  }

  title = 'cancionesArtista';
  
  getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
    results = regex.exec(location.search);
    let prueba = results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    return prueba;
  } 


}
