import { Component, OnInit } from '@angular/core';
import { ArtistService } from '../../services/artist.service';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css']
})
export class HistoryComponent implements OnInit {

  constructor(private artistService: ArtistService) { }

  get history(){
    return this.artistService.history;
  }

  ngOnInit(): void {
  }

}
