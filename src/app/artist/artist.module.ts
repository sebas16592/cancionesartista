import {NgModule} from '@angular/core';

import { HistoryComponent } from './history/history.component';
import { ArtistPageComponent } from './artist-page/artist-page.component';
import { SearchComponent } from './artist-page/search/search.component';
import { ResultComponent } from './artist-page/result/result.component';
import { InformationComponent } from './artist-page/information/information.component';
import { MaterialModule } from '../material/material.module';

@NgModule({
  declarations: [HistoryComponent, ArtistPageComponent, SearchComponent, ResultComponent, InformationComponent],
  exports: [
    HistoryComponent,
    ArtistPageComponent,    
  ],
  imports: [
    MaterialModule
  ],
  bootstrap:[
    InformationComponent,
    ResultComponent
  ]
})
export class ArtistModule { }
