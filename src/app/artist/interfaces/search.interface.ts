import { Followers } from './artist.interface';

export interface Search {
    id?: string;
    image?: string;
    name?: string;
    followers?: Followers;
    genres?: string[];
    url?:string 
}