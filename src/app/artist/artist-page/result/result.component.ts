import { Component } from '@angular/core';
import { ArtistService } from '../../../services/artist.service';

export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
}

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.css']
})
export class ResultComponent {

  constructor(private artistService: ArtistService) { }

  get tracks(){
    
    if (this.artistService.tracks === undefined) {
      return;
    }
        
    let prueba = this.artistService.tracks.tracks.sort((a, b) => (a.name > b.name) ? 1 : -1);
    return prueba;
  }

  displayedColumns: string[] = ['name'];
  dataSource = this.tracks;

}
