import { Component } from '@angular/core';
import { ArtistService } from '../../../services/artist.service';

@Component({
  selector: 'app-information',
  templateUrl: './information.component.html',
  styleUrls: ['./information.component.css']
})
export class InformationComponent{

  constructor(private artistService:ArtistService) { }

  get artist(){
    return this.artistService.artist;
  }

}
