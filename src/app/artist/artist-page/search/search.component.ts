import { Component, ElementRef, ViewChild } from '@angular/core';
import {FormControl} from '@angular/forms';

import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import { ArtistService } from '../../../services/artist.service';
import { Search } from '../../interfaces/search.interface';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent {

  constructor(private artistService:ArtistService) { }

  @ViewChild('txtSearch') txtSearch: ElementRef<HTMLInputElement>;
  @ViewChild('txtSearch2') txtSearch2: ElementRef<HTMLInputElement>;

  myControl = new FormControl()
  public artist:Search;
  public options:Search[];
  filteredOptions: Observable<Search[]>;

  search() {
    const value = this.txtSearch.nativeElement.value;
    if (value.trim().length ===0) {
      return;
    }

    this.artistService.searchArtist(value);
    this.options = this.artistService.result;
    
    this.filteredOptions = this.myControl.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filter(value))
      );
  }

  private _filter(value: string): Search[] {
    const filterValue = value.toLowerCase();

    return this.options.filter(option => option.name.toLowerCase().includes(filterValue));
  }

  searchSounds(){  
    this.artist = this.options.find(element => { 
      return element.name == this.txtSearch.nativeElement.value
    });
    this.artistService.searchAdd(this.artist);  
    this.artistService.searchArtinstSound(this.artist.id);
  }

}
